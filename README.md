
# OpenHealth (PHP)

`OpenHealth` is written in PHP `laravel 5.8`. It has a built-in web server and requires a MySQL compatible database.

This document provides installation instructions for a development setup.

## Dependencies

For local development ensure following dependencies are met:
- Php 7.1.20
- MySQL 5.7

For development with `docker` and `docker-compose` you need, well:
- Docker 18+ `docker --version`
- docker-compose 1.23+ `docker-compose --version`

In any case you will need:
- Git
